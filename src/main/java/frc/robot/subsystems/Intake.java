/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.buttons.JoystickButton;

//abolish the kelton, abolish the dictator, down with tyrany!!!
/**
 * Add your docs here. Add your *GOOGLE* docs here.d
 */

// TODO we need a routine to deploy the intake
// function like deployIntake
//

public class Intake extends SubsystemBase {
  private static Intake m_instance;
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  public boolean intakeBallCount;
  private boolean m_isFull = false;
  private final WPI_TalonSRX m_intakeMotor = new WPI_TalonSRX(Constants.canAddr_intake);

  private Intake() {
    m_intakeMotor.clearStickyFaults();
    m_intakeMotor.setInverted(false);
    register();
  }

  public void isHopperFull(final boolean isFull) {
    m_isFull = isFull;
  }

  @Override
  public void periodic() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
    updateMode();
    periodicOpenLoop();
  }

  public static Intake getInstance() {
    if (m_instance == null) {
      m_instance = new Intake();
    }
    return m_instance;
  }

  private enum IntakeState {
    CMD_OFF__NOT_FULL, CMD_OFF__FULL, CMD_ON__NOT_FULL, CMD_ON__FULL;
  }

  private IntakeState m_state = IntakeState.CMD_OFF__NOT_FULL;

  private boolean shouldWeRunIntake() {
    boolean runIntake = false;

    switch (m_state) {
    case CMD_OFF__NOT_FULL:
      if (m_Mode == IntakeMode.AUTO || m_Mode == IntakeMode.MANUAL_RUN) {
        m_state = IntakeState.CMD_ON__NOT_FULL;
      } else if (m_isFull) {
        m_state = IntakeState.CMD_OFF__FULL;
      }
      runIntake = false;
      break;

    case CMD_OFF__FULL:
      if (m_Mode == IntakeMode.MANUAL_RUN) {
        m_state = IntakeState.CMD_ON__FULL;
      } else if (!m_isFull) {
        m_state = IntakeState.CMD_OFF__NOT_FULL;
      }
      runIntake = false;
      break;

    case CMD_ON__NOT_FULL:
      if (m_Mode == IntakeMode.MANUAL_STOP) {
        m_state = IntakeState.CMD_OFF__NOT_FULL;
      } else if (m_isFull && m_Mode == IntakeMode.AUTO) {
        m_state = IntakeState.CMD_OFF__FULL;
      }
      runIntake = true;
      break;

    case CMD_ON__FULL:
      if (m_Mode == IntakeMode.AUTO || m_Mode == IntakeMode.MANUAL_STOP) {
        m_state = IntakeState.CMD_OFF__FULL;
      }
      runIntake = true;
      break;
    default:
      m_state = IntakeState.CMD_OFF__NOT_FULL;
      break;
    }
    return runIntake;
  }

  final double cmdPercentPower = 1.0;
  private JoystickButton m_autoButton;
  private JoystickButton m_ManualRun;
  private JoystickButton m_ManualStop;

  private enum IntakeMode {
    AUTO, MANUAL_RUN, MANUAL_STOP;
  }

  private IntakeMode m_Mode = IntakeMode.AUTO;

  public void periodicOpenLoop() {
    if (shouldWeRunIntake() && (Math.abs(cmdPercentPower) > 0.05)) {
      m_intakeMotor.set(cmdPercentPower);
    } else {
      m_intakeMotor.set(0);
    }
  }

  public void setAutoButton(JoystickButton joystickButton) {
    m_autoButton = joystickButton;
  }

  public void setManualRun(JoystickButton joystickButton) {
    m_ManualRun = joystickButton;

  }

  public void setManualStop(JoystickButton joystickButton) {
    m_ManualStop = joystickButton;
  }

  private void updateMode() {
    if (m_ManualStop.get()) {
      // set mode manual stop
      m_Mode = IntakeMode.MANUAL_STOP;
    } else if (m_ManualRun.get()) {
      // set mode manual run
      m_Mode = IntakeMode.MANUAL_RUN;
    } else if (m_autoButton.get()) {
      // set mode auto
      m_Mode = IntakeMode.AUTO;
    } else {
      // keep my current mode
    }
  }

}
