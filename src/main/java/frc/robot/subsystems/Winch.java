/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import org.ejml.equation.Variable;

import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.RobotController;

public class Winch extends SubsystemBase {
  private static Winch m_instance;
  private WPI_TalonSRX m_winchMotor = new WPI_TalonSRX(Constants.canAddr_winch);
  private XboxController m_gunnerstick;
  private Timer m_clawCloseTimer = new Timer();

  /**
   * Creates a new Winch.
   */
  // TODO
  public Winch() {
    m_winchMotor.clearStickyFaults();
    m_clawCloseTimer.start();
    register();
  }

  public static Winch getInstance() {
    if (m_instance == null) {
      m_instance = new Winch();
    }
    return m_instance;
  }

  public void setController(XboxController gunnerstick) {
    m_gunnerstick = gunnerstick;
  }

  private final double robotWeight_lbs = 150.0; // TODO
  private final double spoolDiameter_In = 2.0; // TODO
  private final double spoolDiameter_Ft = spoolDiameter_In / 12.0;
  private final double spoolCircumference_Ft = spoolDiameter_Ft * Math.PI;
  private final double gearRatio = 160.0;
  private final double motorPower_Watts = 380.0;
  private final double winchEfficiency = 0.75; // TODO
  private final double motorResitance_ohms = 0.38;
  private double m_winchSpeed_fps = 0.0;

  private void setWinchUnloadedSpeed(final double speed_fps) {
    m_winchSpeed_fps = speed_fps;
    final double volts_Pcnt = calculateWinchVoltsFromUnloadedSpeed(speed_fps);
    m_winchMotor.set(volts_Pcnt);
  }

  private double calculateWinchVoltsFromUnloadedSpeed(final double speed_fps) {
    final double K_e_unloaded = 12.0 / 700.0; // V.s/revs/hlowrld
    final double spoolAxleSpeed_revPerSec = speed_fps / spoolCircumference_Ft;
    final double motorSpeed_revPerSec = spoolAxleSpeed_revPerSec * gearRatio;
    final double volts = K_e_unloaded * motorSpeed_revPerSec;
    final double volts_Pcnt = volts / RobotController.getBatteryVoltage();
    return volts_Pcnt;
  }

  private void setWinchLoadedSpeed(final double speed_fps) {
    m_winchSpeed_fps = speed_fps;
    final double volts_Pcnt = calculateWinchVoltsFromLoadedSpeed(speed_fps);
    m_winchMotor.set(volts_Pcnt);
  }

  private double calculateWinchVoltsFromLoadedSpeed(final double speed_fps) {
    final double torque = spoolDiameter_Ft / 2.0 * robotWeight_lbs / gearRatio / winchEfficiency; // need to convert
                                                                                                  // torque into inches
    final double K_e_T = 12.0 / 700.0 * 6.2 / (6.2 - torque); // V.s/revs
    final double K_T = 6.2 / 130.0; // lbs.in/A
    final double spoolAxleSpeed_revPerSec = speed_fps / spoolCircumference_Ft;
    final double motorSpeed_revPerSec = spoolAxleSpeed_revPerSec * gearRatio;
    final double K_volts = Math.sqrt(motorResitance_ohms * K_e_T / K_T * torque);
    final double volts = K_volts * Math.sqrt(motorSpeed_revPerSec);
    final double volts_Pcnt = volts / RobotController.getBatteryVoltage();
    return volts_Pcnt;
  }

  private void setWinchSpeed(final double Speed) {
    SmartDashboard.putNumber("Speed Loaded Answer", calculateWinchVoltsFromLoadedSpeed(2 * Speed));
    SmartDashboard.putNumber("Speed UnLoaded Answer", calculateWinchVoltsFromUnloadedSpeed(2 * Speed));
    if (Math.abs(Speed) < 0.1) {
      m_winchMotor.set(0);
      return;
    }
    final double batteryVoltCompensatedSpeed = Speed * 12.0 / RobotController.getBatteryVoltage();
    m_winchMotor.set(batteryVoltCompensatedSpeed);
  }

  private boolean m_lightSaberHasEverBeenDeployed = false;
  private JoystickButton m_clawCloseButton;

  private double reelUp() {
    double trigger = m_gunnerstick.getTriggerAxis(Hand.kLeft);
    if (!RobotState.isOperatorControl() || RobotState.isDisabled()) {
      m_lightSaberHasEverBeenDeployed = false;
    }
    if (!okayToWinchUp()) {
      trigger = 0;
    }
    return trigger;
  }

  private boolean okayToWinchUp() {
    // For big claw, need to see if we closed
    return m_clawIsClosed;
    // if we change to a carabineer, as long as the lightsabers have been up, we are
    // good.
    // return m_lightSaberHasEverBeenDeployed |=
    // Lightsaber.getInstance().isLightsaberDeployed();

  }

  private double reelDown() {
    final double trigger = m_gunnerstick.getTriggerAxis(Hand.kRight);
    // TODO - to reel down __ during lightsaber deploy? also in test ~~~ unwind @
    // match speed of trigger going up
    return trigger;
  }

  // otto=big boi
  @Override
  public void periodic() {
    final double winchMinimumSpeed = 0.07;

    double winchSpeed = closeClaw();
    if (winchSpeed < winchMinimumSpeed) {
      winchSpeed = reelUp();
    }
    if (winchSpeed < winchMinimumSpeed) {
      winchSpeed = reelDown();
      if (winchSpeed < winchMinimumSpeed) {
        winchSpeed = 0.0;
      }
    }
    setWinchSpeed(winchSpeed);
    // This method will be called once per scheduler run
  }

  private boolean m_oldClawCloseButton = false;
  private int m_clawButtonPressCount = 0;
  private boolean m_clawIsClosed = false;

  // need a function that will cause the lightsaber claw to grab
  // 1) monitor a button, which will close the claw
  // 2) closing the claw is done by reeling up, using the winch motor
  // 3) reel down for TBD (2?) seconds after the button transitions from false to
  // true
  // 4) only allow the operator to close claws once (unless we are in test or go
  // to disabled then back to teleop)
  // 5) ignore attempts to close, if the lightsaber is not deployed
  private double closeClaw() {
    final boolean clawCloseButton = m_clawCloseButton.get();
    final boolean clawCloseButtonEdgeDetected = m_oldClawCloseButton != clawCloseButton;
    final boolean clawClosedButtonEdgeOn = clawCloseButtonEdgeDetected && clawCloseButton;
    final boolean safeToCloseClaw = Lightsaber.getInstance().isLightsaberDeployed();
    final boolean closeTheClaw = safeToCloseClaw && clawClosedButtonEdgeOn;
    m_oldClawCloseButton = clawCloseButton;
    if (closeTheClaw) {
      if (++m_clawButtonPressCount <= 1) {
        m_clawCloseTimer.reset();
        m_clawIsClosed = true;
      }
    }

    if (RobotState.isTest() || RobotState.isDisabled()) {
      m_clawButtonPressCount = 0;
    }

    if ((m_clawCloseTimer.get() <= 2) && safeToRunWinchMotors()) {
      return 0.5;
    } else {
      return 0.0;
    }
  }

  private boolean safeToRunWinchMotors() {
    final boolean inLast30SecOfTeleop = m_clawCloseTimer.getMatchTime() < 30 && RobotState.isOperatorControl();
    final boolean inTest = RobotState.isTest();
    return inLast30SecOfTeleop || inTest;
  }

  public void setClawCloseButton(JoystickButton joystickButton) {
    m_clawCloseButton = joystickButton;
  }

}
