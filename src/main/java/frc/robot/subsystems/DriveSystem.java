/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.smartdashboard.*;

import com.revrobotics.*;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Timer;

import com.revrobotics.CANSparkMaxLowLevel.MotorType;

//TODO have to do our autonomous routines
// 

/**
 * Add your docs here.
 */
public class DriveSystem extends SubsystemBase {
  public static final String TANKDRIVE_LEFT = "Tankdrive_Left";
  public static final String TANKDRIVE_RIGHT = "Tankdrive_Right";
  private Joystick m_leftStick;
  private Joystick m_rightStick;
  private final double slowspeed = 0.45;
  private double speedFilter = slowspeed;
  private final double gain = 0.04;
  private JoystickButton m_fastspeedRight;
  private JoystickButton m_fastspeedLeft;

  private static CANSparkMax driveTrainLeftOne;
  private static CANSparkMax driveTrainLeftTwo;
  private static CANSparkMax driveTrainRightOne;
  private static CANSparkMax driveTrainRightTwo;
  private static SpeedControllerGroup driveTrainLeftGroup;
  private static SpeedControllerGroup driveTrainRightGroup;
  private static DifferentialDrive driveTrainDifferentialDrive;

  private final double wheelTrack_in = 21.875; // center to center
  private final double wheelBase_in = 23.5; // Front wheel center to rear wheel center
  private final double halfWheelBase_in = 11.5; // Front center to middle axle
  private final double wheelWidth_in = 1.0; // Tire width
  private UltrasonicSensor ultrasonic_X = new UltrasonicSensor(Constants.ai_ultraSonic_X);
  private UltrasonicSensor ultrasonic_Y = new UltrasonicSensor(Constants.ai_ultraSonic_Y);
  private JoystickButton m_reverseToggle;
  private boolean oldButtonPressed = false;
  private boolean m_fwdRev = false;
  private final Timer m_TimerFwdRev = new Timer();
  private CANPIDController m_leftMotorPid;
  private CANPIDController m_rightMotorPid;
  private double kP;
  private double kI;
  private int kD;
  private int kIz;
  private int kFF;
  private int kMaxOutput;
  private int kMinOutput;
  private int maxRPM;

  static DriveSystem m_instance = null;

  public static DriveSystem getInstance() {
    if (m_instance == null) {
      m_instance = new DriveSystem();
    }
    return m_instance;
  }

  private DriveSystem() {
    // Drive declarations
    // CAN BUS
    driveTrainLeftOne = new CANSparkMax(Constants.canAddr_leftFrontDrive, CANSparkMaxLowLevel.MotorType.kBrushless);
    driveTrainLeftTwo = new CANSparkMax(Constants.canAddr_leftRearDrive, CANSparkMaxLowLevel.MotorType.kBrushless);
    driveTrainLeftGroup = new SpeedControllerGroup(driveTrainLeftOne, driveTrainLeftTwo);
    driveTrainRightOne = new CANSparkMax(Constants.canAddr_rightFrontDrive, CANSparkMaxLowLevel.MotorType.kBrushless);
    driveTrainRightTwo = new CANSparkMax(Constants.canAddr_rightRearDrive, CANSparkMaxLowLevel.MotorType.kBrushless);
    driveTrainRightGroup = new SpeedControllerGroup(driveTrainRightOne, driveTrainRightTwo);

    driveTrainDifferentialDrive = new DifferentialDrive(driveTrainLeftGroup, driveTrainRightGroup);

    driveTrainLeftOne.clearFaults();
    driveTrainLeftTwo.clearFaults();
    driveTrainRightOne.clearFaults();
    driveTrainRightTwo.clearFaults();
    driveTrainLeftOne.restoreFactoryDefaults();
    driveTrainLeftTwo.restoreFactoryDefaults();
    driveTrainRightOne.restoreFactoryDefaults();
    driveTrainRightTwo.restoreFactoryDefaults();

    driveTrainRightOne.follow(driveTrainRightTwo);
    driveTrainLeftOne.follow(driveTrainLeftTwo);
    driveTrainRightOne.setInverted(false);
    driveTrainRightTwo.setInverted(false);
    driveTrainLeftOne.setInverted(false);
    driveTrainLeftTwo.setInverted(false);
    driveTrainLeftOne.setMotorType(MotorType.kBrushless);
    driveTrainLeftTwo.setMotorType(MotorType.kBrushless);
    driveTrainRightOne.setMotorType(MotorType.kBrushless);
    driveTrainRightTwo.setMotorType(MotorType.kBrushless);

    m_leftEncoder = driveTrainLeftOne.getEncoder();
    m_rightEncoder = driveTrainRightOne.getEncoder();

    m_TimerFwdRev.reset();
    m_TimerFwdRev.start();

    Vision.getInstance();

    /**
     * In order to use PID functionality for a controller, a CANPIDController object
     * is constructed by calling the getPIDController() method on an existing
     * CANSparkMax object
     */
    m_leftMotorPid = driveTrainLeftOne.getPIDController();
    m_rightMotorPid = driveTrainRightOne.getPIDController();

    // Encoder object created to display position values
    m_leftEncoder = driveTrainLeftOne.getEncoder();
    m_rightEncoder = driveTrainRightOne.getEncoder();

    // PID Velocity control coefficients
    kP = 5e-5;
    kI = 1e-6;
    kD = 0;
    kIz = 0;
    kFF = 0;
    kMaxOutput = 1;
    kMinOutput = -1;
    maxRPM = 5700;

    // set Velocity PID coefficients
    m_leftMotorPid.setP(kP, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setI(kI, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setD(kD, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setIZone(kIz, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setFF(kFF, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setOutputRange(kMinOutput, kMaxOutput, PIDMode.PID_VELOCITY.value);

    m_rightMotorPid.setP(kP, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setI(kI, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setD(kD, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setIZone(kIz, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setFF(kFF, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setOutputRange(kMinOutput, kMaxOutput, PIDMode.PID_VELOCITY.value);

    register();
  }

  /**
   * @param turnRadius_in the radius we want to turn in in inches from the center
   *                      of the robot
   *                      <p>
   *                      a right turn is a positive "r" and a left turn is a
   *                      negative "r"
   *                      <p>
   *                      an "r" of zero means to pivot in place
   * @return returns the ratio of the (right side wheel speed) divided by the
   *         (left side wheel speed)
   */
  private double wheelSpeedRatioForCircularTrack(final double turnRadius_in) {
    if (Double.isInfinite(turnRadius_in)) {
      return 1.0;
    } else if (turnRadius_in == -wheelTrack_in / 2.0) {
      return Double.POSITIVE_INFINITY;
    }
    return (turnRadius_in - wheelTrack_in / 2.0) / (turnRadius_in + wheelTrack_in / 2.0);
  }

  private double getRightWheelSpeed(final double speedRatio, final double maxSpeed) {
    if (Double.isInfinite(speedRatio)) {
      return maxSpeed;

    } else if (Math.abs(speedRatio) == 1.0) {
      return maxSpeed;
    } else if (speedRatio == 0.0) {
      return 0.0;
    } else {
      final double speedRightAbnormal = (2.0 * maxSpeed * speedRatio) / (1.0 + speedRatio);
      final double speedLeftAbnormal = (2.0 * maxSpeed) / (1.0 + speedRatio);
      double normalized = 1.0;
      if (Math.abs(speedRightAbnormal) > 1.0) {
        normalized = 1.0 / speedRightAbnormal;
      } else if (Math.abs(speedLeftAbnormal) > 1.0) {
        normalized = 1.0 / speedLeftAbnormal;
      }
      return speedRightAbnormal * Math.abs(normalized);

    }
  }

  /**
   * wheelRev calculates the number of revolutions a wheel must make in order for
   * it to for a point on its surface to move the provided linear distance. For
   * drive, this is the number of times the wheel turns while driving the input
   * distance
   * 
   * @param distance_Inches linear distance to traverse, in feet
   * @return number of wheel revolutions
   */
  private double wheelRev(final double distance_Inches) {
    final double wheelDiameter_Inches = 6.0;
    final double wheelCircumference_Inches = wheelDiameter_Inches * Math.PI; // gary's wheels are six in.
    return distance_Inches / wheelCircumference_Inches;
  }

  private double motorRevs(final double wheelRev) {
    final double GearRatio = 12.75; // gary's wheel ratio.
    return wheelRev * GearRatio;
  }

  private void teleopDriveFwdRev() {
    final boolean buttonPressed = m_reverseToggle.get();
    final boolean buttonEdged = buttonPressed && !oldButtonPressed;
    final boolean buttonEdgedOn = buttonEdged && buttonPressed;
    oldButtonPressed = buttonPressed;

    if (buttonEdgedOn) {
      m_fwdRev = !m_fwdRev;
      m_TimerFwdRev.reset();
      // Vision.getInstance().setMainCamera(m_fwdRev ? Vision.CameraSource.REV :
      // Vision.CameraSource.FWD);
    }
  }

  public enum PIDMode {
    OPEN_LOOP(0), PID_VELOCITY(1), PID_POSITION(2), PID_VOLTAGE(3);

    public final int value;

    private PIDMode(final int value) {
      this.value = value;
    }
  };

  private PIDMode m_mode = PIDMode.PID_VELOCITY;
  private CANEncoder m_leftEncoder;
  private CANEncoder m_rightEncoder;
  private boolean m_runningAutoDrive;

  private double getLeftWheelSpeed(final double speedRatio, final double rightWheelSpeed) {
    return rightWheelSpeed / speedRatio;
  }

  private double getRightWheelDistance(final double driveDistance_in, final double speedRatio) {
    return (2 * speedRatio * driveDistance_in) / (1 + speedRatio);
  }

  private double getLeftWheelDistance(final double driveDistance_in, final double speedRatio) {
    return (2 * driveDistance_in) / (1 + speedRatio);
  }

  @Override
  public void periodic() {
    if (RobotState.isEnabled()) {
      if (RobotState.isAutonomous()) {
        autonomousPeriodic();
      } else if (RobotState.isOperatorControl()) {
        teleOpPeriodic();
      }
    }
  }

  private void teleOpPeriodic() {
    teleopDriveFwdRev();
    final boolean fast = m_fastspeedLeft.get() || m_fastspeedRight.get();
    double speed = slowspeed;
    // run slow if slow trigger is not pressed or if the direction toggle was just
    // changed
    if (fast && m_TimerFwdRev.get() > 2.0) {
      speed = 1;
    }

    speedFilter = speedFilter + gain * (speed - speedFilter);

    final double leftYRaw = m_leftStick.getY();
    final double rightYRaw = m_rightStick.getY();
    final double fwdRev = m_fwdRev ? -1 : 1;
    final double leftSide = fwdRev * leftYRaw * speedFilter;
    final double rightSide = fwdRev * rightYRaw * speedFilter;
    // double ls = leftSide * leftSide * leftSide;
    // double rs = rightSide * rightSide * rightSide;
    tankDrive(leftSide, rightSide);
  }

  public void tankDrive(double left, double right) {
    driveTrainDifferentialDrive.tankDrive(left, right);
  }

  public void setLeftDriveStick(final Joystick leftDrivestick) {
    m_leftStick = leftDrivestick;
  }

  public void setRightDriveStick(final Joystick rightDrivestick) {
    m_rightStick = rightDrivestick;
  }

  public void setfastspeedright(final JoystickButton fastspeedright) {
    m_fastspeedRight = fastspeedright;
  }

  public void setfastspeedleft(final JoystickButton fastspeedleft) {
    m_fastspeedLeft = fastspeedleft;
  }

  public void setReverseToggle(final JoystickButton reverseToggle) {
    m_reverseToggle = reverseToggle;
  }

  public boolean autonomousBusy() {
    return m_runningAutoDrive;
  }

  public void newAutonomousCommand(final double turnRadius_inches, final double tgtSpeed_InPerSec,
      final double driveDistance_Inches) {
    final double speedRatio = wheelSpeedRatioForCircularTrack(turnRadius_inches);
    final double rightWheelSpeed_InPerSec = getRightWheelSpeed(speedRatio, tgtSpeed_InPerSec);
    final double leftWheelSpeed_InPerSec = getLeftWheelSpeed(speedRatio, rightWheelSpeed_InPerSec);
    final double rightWheelDistance_inches = getRightWheelDistance(driveDistance_Inches, speedRatio);
    final double leftWheelDistance_inches = getLeftWheelDistance(driveDistance_Inches, speedRatio);

    final double right_Rpm = motorRevs(wheelRev(rightWheelSpeed_InPerSec)) * 60.0;
    final double left_Rpm = motorRevs(wheelRev(leftWheelSpeed_InPerSec)) * 60.0;

    final double minPercentRpm = 0.05;
    final double minRpm = minPercentRpm * maxRPM;
    final double rightLimited_Rpm = right_Rpm < minRpm ? 0.0 : right_Rpm;
    final double leftLimited_Rpm = left_Rpm < minRpm ? 0.0 : left_Rpm;

    if (rightLimited_Rpm != 0.0) {
      m_rightMotorPid.setReference(rightLimited_Rpm, ControlType.kVelocity, PIDMode.PID_VELOCITY.value);
    } else {
      m_rightMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
    }
    m_rightEncoder.setPosition(0);

    if (leftLimited_Rpm != 0.0) {
      m_leftMotorPid.setReference(leftLimited_Rpm, ControlType.kVelocity, PIDMode.PID_VELOCITY.value);
    } else {
      m_leftMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
    }
    m_leftEncoder.setPosition(0);

    m_rightWheelDistance_Revs = motorRevs(wheelRev(rightWheelDistance_inches));
    m_leftWheelDistance_Revs = motorRevs(wheelRev(leftWheelDistance_inches));
    m_runningAutoDrive = true;
  }

  private double m_rightWheelDistance_Revs;
  private double m_leftWheelDistance_Revs;

  private void autonomousPeriodic() {
    if (m_rightWheelDistance_Revs >= m_rightEncoder.getPosition()
        || m_leftWheelDistance_Revs >= m_leftEncoder.getPosition()) {
      m_rightMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_leftMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_runningAutoDrive = false;
    }
    testperiodicVelocity();
  }

  private void testperiodicVelocity() {
    if (!RobotState.isTest()) {
      // return;
    }
    final double motorRpm = m_leftEncoder.getVelocity();
    // SmartDashboard.putNumber("Velocity SetPoint", setPoint);
    SmartDashboard.putNumber("Velocity", motorRpm);
    // SmartDashboard.putNumber("Velocity Error", setPoint - motorRpm);
  }

}
