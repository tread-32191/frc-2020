
/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.RobotState;

/**
 * Ultrasonic sensor cannot detect anything less than 11 inches works from 20 up
 * to 100 in.
 */
public class UltrasonicSensor extends SubsystemBase {
    // factor to convert sensor values to a distance in inches
    private static final double kValueToInches = (0.125 * 39) / 97.5;

    private int m_kUltrasonicPort;
    private final AnalogInput m_ultrasonic;
    private double currentDistance_Inches = 0;

    public UltrasonicSensor(final int kUltrasonicPort) {
        m_kUltrasonicPort = kUltrasonicPort;
        m_ultrasonic = new AnalogInput(kUltrasonicPort);
        register();
    }

    @Override
    public void periodic() {
        currentDistance_Inches = m_ultrasonic.getValue() * kValueToInches;
        testPeriodic();
    }

    /**
     * 
     * @return distance, in inches, to the target
     */
    public double get() {
        return currentDistance_Inches;
    }

    private void testPeriodic() {
        if (!RobotState.isTest()) {
            return;
        }
        final String label = "Ultrasonic Sensor" + (m_kUltrasonicPort == Constants.ai_ultraSonic_X ? " X" : " Y");
        SmartDashboard.putNumber(label, currentDistance_Inches);
    }
}
