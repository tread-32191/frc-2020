/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.Encoder;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.AnalogGyro;

/**
 * This is a demo program showing the use of the RobotDrive class, specifically
 * it contains the code necessary to operate a robot with tank drive.
 */
// TODO need to figure out if we will need to raise or lower the angle of the
// shooter
//

public class ShooterOld extends SubsystemBase {
  private final WPI_TalonSRX m_angleControl = new WPI_TalonSRX(Constants.canAddr_shooterElevator);

  private CANSparkMax m_leftMotor;
  private CANPIDController m_leftMotorPid;
  private CANEncoder m_leftEncoder;
  private double kP, kI, kD, kIz, kFF, kMaxOutput, kMinOutput, maxRPM;
  private static ShooterOld m_instance = null;
  double highShotSpeed_Pcnt = 100.0;
  double lowShotSpeed_Pcnt = 40.0;
  private Encoder shooterElevationEnc = new Encoder(0, 1);

  public static ShooterOld getInstance() {
    if (m_instance == null) {
      m_instance = new ShooterOld();
    }
    return m_instance;
  }

  public enum PIDMode {
    OPEN_LOOP(0), PID_VELOCITY(1), PID_POSITION(2), PID_VOLTAGE(3);

    public final int value;

    private PIDMode(final int value) {
      this.value = value;
    }
  };

  private PIDMode m_mode = PIDMode.PID_VELOCITY;

  private double driveDistance_Feet = 0.0;
  private CANSparkMax m_rightMotor;
  private CANPIDController m_rightMotorPid;
  private CANEncoder m_rightEncoder;
  private JoystickButton m_HighShot;
  private JoystickButton m_LowShot;

  private ShooterOld() {

    // initialize Neo motor
    m_angleControl.clearStickyFaults(0);
    m_angleControl.configFactoryDefault();
    m_leftMotor = new CANSparkMax(Constants.canAddr_leftShooter, MotorType.kBrushless);
    m_rightMotor = new CANSparkMax(Constants.canAddr_rightShooter, MotorType.kBrushless);

    /**
     * The RestoreFactoryDefaults method can be used to reset the configuration
     * parameters in the SPARK MAX to their factory default state. If no argument is
     * passed, these parameters will not persist between power cycles
     */
    m_leftMotor.restoreFactoryDefaults();
    m_rightMotor.restoreFactoryDefaults();
    m_leftMotor.setIdleMode(IdleMode.kBrake);
    m_rightMotor.setIdleMode(IdleMode.kBrake);

    // m_rightMotor.follow(m_leftMotor);

    m_rightMotor.setInverted(false);
    m_leftMotor.setInverted(false);

    /**
     * In order to use PID functionality for a controller, a CANPIDController object
     * is constructed by calling the getPIDController() method on an existing
     * CANSparkMax object
     */
    m_leftMotorPid = m_leftMotor.getPIDController();
    m_rightMotorPid = m_rightMotor.getPIDController();

    // Encoder object created to display position values
    m_leftEncoder = m_leftMotor.getEncoder();
    m_rightEncoder = m_rightMotor.getEncoder();

    // PID Velocity control coefficients
    kP = 5e-5;
    kI = 1e-6;
    kD = 0;
    kIz = 0;
    kFF = 0;
    kMaxOutput = 1;
    kMinOutput = -1;
    maxRPM = 5700;

    // set Velocity PID coefficients
    m_leftMotorPid.setP(kP, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setI(kI, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setD(kD, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setIZone(kIz, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setFF(kFF, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setOutputRange(kMinOutput, kMaxOutput, PIDMode.PID_VELOCITY.value);

    m_rightMotorPid.setP(kP, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setI(kI, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setD(kD, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setIZone(kIz, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setFF(kFF, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setOutputRange(kMinOutput, kMaxOutput, PIDMode.PID_VELOCITY.value);

    // set Velocity PID coefficients
    final double kP_pos = 0.05;
    final double kI_pos = 1e-6;
    final double kD_pos = 2e-4;
    final double fIz_pos = 10.0;
    m_leftMotorPid.setP(kP_pos, PIDMode.PID_POSITION.value);
    m_leftMotorPid.setI(kI_pos, PIDMode.PID_POSITION.value);
    m_leftMotorPid.setD(kD_pos, PIDMode.PID_POSITION.value);
    m_leftMotorPid.setIZone(fIz_pos, PIDMode.PID_POSITION.value);
    m_leftMotorPid.setFF(0, PIDMode.PID_POSITION.value);
    m_leftMotorPid.setOutputRange(-1.0, 1.0, PIDMode.PID_POSITION.value);

    m_rightMotorPid.setP(kP_pos, PIDMode.PID_POSITION.value);
    m_rightMotorPid.setI(kI_pos, PIDMode.PID_POSITION.value);
    m_rightMotorPid.setD(kD_pos, PIDMode.PID_POSITION.value);
    m_rightMotorPid.setIZone(kIz, PIDMode.PID_POSITION.value);
    m_rightMotorPid.setFF(0, PIDMode.PID_POSITION.value);
    m_rightMotorPid.setOutputRange(-1.0, 1.0, PIDMode.PID_POSITION.value);

    register();
  }

  @Override
  public void periodic() {
    periodicPidTuning();
    final PIDMode newMode = getMode();
    if (newMode != m_mode) {
      m_leftMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_mode = newMode;
    }

    if (m_mode == PIDMode.PID_VELOCITY) {
      periodicVelocity();
    } else if (m_mode == PIDMode.PID_POSITION) {
      periodicPosition();
    } else if (m_mode == PIDMode.OPEN_LOOP) {
      periodicOpenLoop();
    } else {
      // ... undefined mode
    }
    setAngleAdjust();
  }

  public void setMode(final PIDMode mode) {
    m_mode = mode;
  }

  public void periodicVelocity() {
    final double minPercentRpm = 0.05;
    double cmdPercentRpm;
    if (m_HighShot.get()) {
      cmdPercentRpm = highShotSpeed_Pcnt;
    } else if (m_LowShot.get()) {
      cmdPercentRpm = lowShotSpeed_Pcnt;
    } else {
      cmdPercentRpm = 0.0;
    }

    /*
     * If the stick command is really close to 0, just return 0; otherwise return
     * the stick command make sure that it works for forward and backward commands,
     * so check the absolute value
     */
    final double setPoint = (Math.abs(cmdPercentRpm) < minPercentRpm ? 0.0 : cmdPercentRpm) / 100.0 * maxRPM;
    SmartDashboard.putNumber("Shooter Setpoint", setPoint);
    if (setPoint != 0.0) {
      m_leftMotorPid.setReference(setPoint, ControlType.kVelocity, PIDMode.PID_VELOCITY.value);
      m_rightMotorPid.setReference(-setPoint, ControlType.kVelocity, PIDMode.PID_VELOCITY.value);
    } else {
      m_leftMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_rightMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
    }
    testperiodicVelocity();
  }

  public void periodicOpenLoop() {
    // todo make this work with buttons
    final double cmdPercentRpm = 0.0;
    if (Math.abs(cmdPercentRpm) > 0.05) {
      m_leftMotor.set(cmdPercentRpm);
      m_rightMotor.set(-1 * cmdPercentRpm);
    }
  }

  public void periodicPosition() {
    // Debug
    final double newDriveDistance_Feet = SmartDashboard.getNumber("Drive Dist (Ft)", driveDistance_Feet);
    if (newDriveDistance_Feet != driveDistance_Feet) {
      driveDistance_Feet = newDriveDistance_Feet;
      m_leftMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_leftEncoder.setPosition(0);
      m_rightMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_rightEncoder.setPosition(0);
    }
    final double wheelRotations = wheelRev(driveDistance_Feet);
    final double motorRotations = motorRevs(wheelRotations);

    m_leftMotorPid.setReference(motorRotations, ControlType.kPosition, PIDMode.PID_POSITION.value);
    testPeriodicPosition();
  }

  /**
   * wheelRev calculates the number of revolutions a wheel must make in order for
   * it to for a point on its surface to move the provided linear distance. For
   * drive, this is the number of times the wheel turns while driving the input
   * distance
   * 
   * @param distanceFt linear distance to traverse, in feet
   * @return number of wheel revolutions
   */
  private double wheelRev(final double distanceFt) {
    double wheelCircumference = 0.5 * Math.PI; // gary's wheels are six in.
    return distanceFt / wheelCircumference;
  }

  private double motorRevs(final double wheelRev) {
    double GearRatio = 10.71; // gary's wheel ratio.
    return wheelRev * GearRatio;
  }

  public void setHighShot(final JoystickButton highShot) {
    m_HighShot = highShot;
  }

  public boolean getIsShooting() {
    return m_HighShot.get() || m_LowShot.get();
  }

  public void setLowShot(final JoystickButton lowShot) {
    m_LowShot = lowShot;
  }

  // Test methods below here
  private PIDMode getMode() {
    if (!RobotState.isTest()) {
      return m_mode;
    }

    final boolean pidVel = SmartDashboard.getBoolean("PID: VEL", m_mode == PIDMode.PID_VELOCITY);
    final boolean pidPos = SmartDashboard.getBoolean("PID: POS", m_mode == PIDMode.PID_POSITION);
    final boolean openVel = SmartDashboard.getBoolean("OPEN VEL", m_mode == PIDMode.OPEN_LOOP);
    if (pidVel && m_mode != PIDMode.PID_VELOCITY) {
      SmartDashboard.putBoolean("PID: POS", false);
      SmartDashboard.putBoolean("OPEN VEL", false);
      return PIDMode.PID_VELOCITY;
    } else if (pidPos && m_mode != PIDMode.PID_POSITION) {
      SmartDashboard.putBoolean("PID: VEL", false);
      SmartDashboard.putBoolean("OPEN VEL", false);
      return PIDMode.PID_POSITION;
    } else if (openVel && m_mode != PIDMode.OPEN_LOOP) {
      SmartDashboard.putBoolean("PID: VEL", false);
      SmartDashboard.putBoolean("PID: POS", false);
      return PIDMode.OPEN_LOOP;
    } else {
      return m_mode;
    }
  }

  private void testPeriodicPosition() {
    if (!RobotState.isTest()) {
      return;
    }
    // SmartDashboard.putNumber("Position Setpoint", m_leftMotorPid);
    SmartDashboard.putNumber("Position", m_leftEncoder.getPosition());
    // SmartDashboard.putNumber("Position Error", motorRotations -
    // m_leftEncoder.getPosition());
  }

  private boolean notRunYet = true;

  private XboxController m_gunnerstick;

  public void periodicPidTuning() {
    if (!RobotState.isTest()) {
      return;
    }
    if (notRunYet) {
      // display PID coefficients on SmartDashboard
      SmartDashboard.putNumber("P Gain", kP);
      SmartDashboard.putNumber("I Gain", kI);
      SmartDashboard.putNumber("D Gain", kD);
      SmartDashboard.putNumber("I Zone", kIz);
      SmartDashboard.putNumber("Feed Forward", kFF);
      SmartDashboard.putNumber("Max Output", kMaxOutput);
      SmartDashboard.putNumber("Min Output", kMinOutput);
      SmartDashboard.putBoolean("PID: VEL", m_mode == PIDMode.PID_VELOCITY);
      SmartDashboard.putNumber("Drive Dist (Ft)", 0.0);

      SmartDashboard.putNumber("highShotSpeed_%", highShotSpeed_Pcnt);
      SmartDashboard.putNumber("lowShotSpeed_%", lowShotSpeed_Pcnt);
      notRunYet = false;
    }
    // read PID coefficients from SmartDashboard
    final double p = SmartDashboard.getNumber("P Gain", 0);
    final double i = SmartDashboard.getNumber("I Gain", 0);
    final double d = SmartDashboard.getNumber("D Gain", 0);
    final double iz = SmartDashboard.getNumber("I Zone", 0);
    final double ff = SmartDashboard.getNumber("Feed Forward", 0);
    final double max = SmartDashboard.getNumber("Max Output", 0);
    final double min = SmartDashboard.getNumber("Min Output", 0);

    // if PID coefficients on SmartDashboard have changed, write new values to
    // controller
    if ((p != kP)) {
      m_leftMotorPid.setP(p);
      kP = p;
    }
    if ((i != kI)) {
      m_leftMotorPid.setI(i);
      kI = i;
    }
    if ((d != kD)) {
      m_leftMotorPid.setD(d);
      kD = d;
    }
    if ((iz != kIz)) {
      m_leftMotorPid.setIZone(iz);
      kIz = iz;
    }
    if ((ff != kFF)) {
      m_leftMotorPid.setFF(ff);
      kFF = ff;
    }
    if ((max != kMaxOutput) || (min != kMinOutput)) {
      m_leftMotorPid.setOutputRange(min, max);
      kMinOutput = min;
      kMaxOutput = max;
    }

    highShotSpeed_Pcnt = SmartDashboard.getNumber("highShotSpeed_%", 0.0) / 100.0;
    lowShotSpeed_Pcnt = SmartDashboard.getNumber("lowShotSpeed_%", 0.0) / 100.0;

  }

  private void testperiodicVelocity() {
    if (!RobotState.isTest()) {
      // return;
    }
    final double motorRpm = m_leftEncoder.getVelocity();
    // SmartDashboard.putNumber("Velocity SetPoint", setPoint);
    SmartDashboard.putNumber("Velocity", motorRpm);
    // SmartDashboard.putNumber("Velocity Error", setPoint - motorRpm);
  }

  private void setAngleAdjust() {
    final double stickAngle = m_gunnerstick.getY();
    if (Math.abs(stickAngle) >= 0.2) {
      m_angleControl.set(stickAngle);
    } else {
      m_angleControl.set(0);
    }
  }

  public void setController(XboxController gunnerstick) {
    m_gunnerstick = gunnerstick;

  }

}