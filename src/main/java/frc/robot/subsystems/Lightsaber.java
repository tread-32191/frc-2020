/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the projectz.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Add your docs here.
 */

public class Lightsaber extends SubsystemBase {
  private static Lightsaber m_instance;
  private final DigitalInput leftFwdLimit = new DigitalInput(Constants.dio_leftLightsaberFwdLimit);
  private final DigitalInput rightFwdLimit = new DigitalInput(Constants.dio_rightLightsaberFwdLimit);
  private final DigitalInput leftBkdLimit = new DigitalInput(Constants.dio_leftLightsaberBkdLimit);
  private final DigitalInput rightBkdLimit = new DigitalInput(Constants.dio_rightLightsaberBkdLimit);
  private final WPI_TalonSRX lightsaber = new WPI_TalonSRX(Constants.canAddr_lightSaber);
  private XboxController m_gunnerstick;
  private final Timer m_Timer = new Timer();

  private Lightsaber() {
    lightsaber.clearStickyFaults(0);
    lightsaber.configFactoryDefault();
    lightsaber.setNeutralMode(NeutralMode.Brake);
    register();

  }

  // Put methods for controlling this subsystem
  // here. Call these from Commands
  public static Lightsaber getInstance() {
    if (m_instance == null) {
      m_instance = new Lightsaber();
    }
    return m_instance;
  }

  // Max paine was here
  @Override
  public void periodic() {
    if (true ||RobotState.isTest() || (m_Timer.getMatchTime() < 30 && RobotState.isOperatorControl())) {
      if (m_gunnerstick.getPOV() == 0) { //} && !isLightsaberDeployed()) {
        lightsaber.set(0.25);
      } else if (m_gunnerstick.getPOV() == 180) { //} && rightBkdLimit.get() == false && leftBkdLimit.get() == false) {
        lightsaber.set(-0.25);
      } else {
        lightsaber.set(0);
      }
    }
    testPeriodic();
  }

  private void testPeriodic() {
    if (!RobotState.isTest()) {
      return;
    }
  }

  public void setController(XboxController gunnerstick) {
    m_gunnerstick = gunnerstick;
  }

  public boolean isLightsaberDeployed() {
    // TODO: should this be based on a high switch?
    // Should this be based on only the fact that the light sabers are not all the
    // way down?

    // For now: All the way Up
    return (rightFwdLimit.get() || leftFwdLimit.get());
  }

}