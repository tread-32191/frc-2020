/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.Encoder;

/**
 * This is a demo program showing the use of the RobotDrive class, specifically
 * it contains the code necessary to operate a robot with tank drive.
 */
// TODO need to figure out if we will need to raise or lower the angle of the
// shooter
//

public class ShooterElevator extends SubsystemBase {
  private final WPI_TalonSRX m_angleControl = new WPI_TalonSRX(Constants.canAddr_shooterElevator);

  private static ShooterElevator m_instance = null;
  double highShotSpeed_Pcnt = 100.0;
  double lowShotSpeed_Pcnt = 40.0;
  private final Encoder shooterElevationEnc = new Encoder(0, 1);
  private XboxController m_gunnerstick;

  private final GyroSensor m_gyro = GyroSensor.getInstance();

  public static ShooterElevator getInstance() {
    if (m_instance == null) {
      m_instance = new ShooterElevator();
    }
    return m_instance;
  }

  private ShooterElevator() {
    // initize Motor Controller
    m_angleControl.clearStickyFaults(0);
    m_angleControl.configFactoryDefault();
    m_angleControl.setInverted(false);
    register();
  }

  @Override
  public void periodic() {
    setAngleAdjust();
  }

  private final double m_maxAngle = 20.0; // TODO
  private final double m_minAngle = -1.0; // TODO

  private boolean m_goToMax = false;

  private boolean m_goToMin = false;

  private void setAngleAdjust() {
    double elevatorPower = m_gunnerstick.getY();
    final double maxPower = 0.99
    ;
    final double minPower = 0.05;

    // Override the stick power command if one of the subsystems needs the elevator
    // to move into position
    if (m_goToMax) {
      elevatorPower = -1 * maxPower;
    } else if (m_goToMin) {
      elevatorPower = maxPower;
    }

    final double shooterAngle_deg = m_gyro.get();
    if (shooterAngle_deg < m_minAngle && elevatorPower < 0.0) {
      elevatorPower = 0.0;
    } else if (shooterAngle_deg > m_maxAngle && elevatorPower > 0.0) {
      elevatorPower = 0.0;
    } else if (Math.abs(elevatorPower) > maxPower) {
      elevatorPower = maxPower * Math.signum(elevatorPower);
    }

    if (Math.abs(elevatorPower) >= minPower) {
      m_angleControl.set(elevatorPower);
    } else {
      m_angleControl.set(0);
    }

    m_goToMax = false;
    m_goToMin = false;
  }

  public void goToMax() {
    m_goToMax = true;
  }

  public void goToMin() {
    m_goToMin = true;
  }

  public void setController(XboxController gunnerstick) {
    m_gunnerstick = gunnerstick;
  }
}