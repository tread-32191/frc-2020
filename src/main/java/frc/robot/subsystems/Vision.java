/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.MjpegServer;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Vision extends SubsystemBase {
  private final String DASHBOARD_CAMERA_NAME = "Camera Name";
  private String hostName = "10.32.19.51";

  private static Vision m_instance;

  public String cameraName() {
    return hostName;
  }

  public void checkHostName() {
    String testName = SmartDashboard.getString(DASHBOARD_CAMERA_NAME, hostName);
    if (this.hostName.equalsIgnoreCase(testName)) {
      this.setHostName(testName);
    }
  }

  public void setHostName(String newName) {
    if (this.hostName.equalsIgnoreCase(newName)) {
      this.hostName = newName;
      SmartDashboard.putString(DASHBOARD_CAMERA_NAME, this.hostName);
    }
  }

  public static Vision getInstance() {
    if (m_instance == null) {
      m_instance = new Vision();
    }
    return m_instance;
  }

  private Vision() {
    new Thread(() -> {
      final double aspectRatio = 0.75;
      final int width = 320;
      final int height = (int) (width * aspectRatio);
      final int pnpWidth = width / 4;
      final int pnpHeight = (int) (pnpWidth * aspectRatio);
      final int pnpColStart = width - pnpWidth;
      final int pnpRowStart = height - pnpHeight;
      final int fps = 15;

      UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
      UsbCamera camera2 = CameraServer.getInstance().startAutomaticCapture();
      camera.setResolution(width, height);
      camera2.setResolution(width, height);
      camera.setFPS(fps);
      camera2.setFPS(fps);
      /*
       * CvSource outputStream = CameraServer.getInstance().putVideo("Blur", width,
       * height);
       * 
       * Mat source = new Mat(); Mat source2 = new Mat(); Mat output = new Mat();
       */
      while (!Thread.interrupted() || m_camSelected != CameraSource.FWD) {
        /*
         * UsbCamera ptrCameraA = camera; UsbCamera ptrCameraB = camera2; if
         * (m_camSelected == CameraSource.REV) { ptrCameraA = camera2; ptrCameraB =
         * camera; }
         * 
         * CvSink cvSink = CameraServer.getInstance().getVideo(ptrCameraA); CvSink
         * cvSink2 = CameraServer.getInstance().getVideo(ptrCameraB); while
         * (cvSink.grabFrame(source) == 0) { }
         * 
         * while (cvSink2.grabFrame(source2) == 0) { }
         * 
         * // Imgproc.cvtColor(source, output, Imgproc.COLOR_BGR2GRAY); Size dsize = new
         * Size(pnpWidth, pnpHeight); Imgproc.resize(source2, output, dsize);
         * output.copyTo(source.rowRange(pnpRowStart, height).colRange(pnpColStart,
         * width)); outputStream.putFrame(source); outputStream.setFPS(fps);
         */
      }
    }).start();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    synchronized (m_camSelected) {
      if (m_camSelected == CameraSource.FWD) {
      } else {
      }
    }

  }

  public enum CameraSource {
    FWD(0), REV(1);

    private final int value;

    private CameraSource(final int value) {
      this.value = value;
    }
  }

  private CameraSource m_camSelected = CameraSource.FWD;

  public void setMainCamera(final CameraSource cam) {
    synchronized (m_camSelected) {
      m_camSelected = cam;
    }
  }
}
