/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class GyroSensor extends SubsystemBase {
  private static GyroSensor m_instance;

  private static final SPI.Port kGyroPort = SPI.Port.kOnboardCS0;
  private ADXRS450_Gyro m_gyro = new ADXRS450_Gyro(kGyroPort);

  private GyroSensor() {
    m_gyro.calibrate();
  }

  public static GyroSensor getInstance() {
    if (m_instance == null) {
      m_instance = new GyroSensor();
    }
    return m_instance;
  }

  @Override
  public void periodic() {
    SmartDashboard.putNumber("Gyro", m_gyro.getAngle());

  }

  public double get() {
    return m_gyro.getAngle();
  }
}
