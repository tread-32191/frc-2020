/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.RobotState;

/**
 * AllenBradleySensor class assume that its black wire is connected to the
 * analog signal. It will not work properly if the white wire is used as the
 * analog input. This configuration is more than a question of polarity... if
 * the battery voltage drops, using the white wire will prevent a voltage drop
 * or a disconnected sensor from indicating presence.
 */
public class AllenBradleySensor extends SubsystemBase {
  private AnalogInput AllenBradleyProximitySensor;
  private int Prox_counter = 0;
  private double oldVoltage = 0;
  final double digitalThreshold_Volts = 0.65; // 650 mV

  public AllenBradleySensor(final int ai_channel) {
    AllenBradleyProximitySensor = new AnalogInput(ai_channel);
    register();
  }

  public void resetCounter() {
    Prox_counter = 0;
  }

  public int getCounter() {
    return Prox_counter;
  }

  @Override
  public void periodic() {
    final double newVoltage = AllenBradleyProximitySensor.getVoltage();
    if ((oldVoltage < digitalThreshold_Volts) && (newVoltage > digitalThreshold_Volts)) {
      Prox_counter++;
    }
    oldVoltage = newVoltage;
    testPeriodic();
  }

  public boolean get() {
    return oldVoltage > digitalThreshold_Volts;
  }

  private void testPeriodic() {
    if (!RobotState.isTest()) {
      return;
    }
    SmartDashboard.putNumber("Ball counter", Prox_counter);
    SmartDashboard.putNumber("Voltage", oldVoltage);
    SmartDashboard.putBoolean("Detected", get()); // true if detected
  }
}