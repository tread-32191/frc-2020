/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

/**
 * This is a demo program showing the use of the RobotDrive class, specifically
 * it contains the code necessary to operate a robot with tank drive.
 */
// TODO need to figure out if we will need to raise or lower the angle of the
// shooter
//

public class Shooter extends SubsystemBase {
  private CANSparkMax m_leftMotor;
  private CANPIDController m_leftMotorPid;
  private CANEncoder m_leftEncoder;
  private double kP, kI, kD, kIz, kFF, kMaxOutput, kMinOutput, maxRPM;
  private static Shooter m_instance = null;
  double highShotSpeed_Pcnt = 100.0;
  double lowShotSpeed_Pcnt = 40.0;

  public static Shooter getInstance() {
    if (m_instance == null) {
      m_instance = new Shooter();
    }
    return m_instance;
  }

  public enum PIDMode {
    OPEN_LOOP(0), PID_VELOCITY(1), PID_POSITION(2), PID_VOLTAGE(3);

    public final int value;

    private PIDMode(final int value) {
      this.value = value;
    }
  };

  private PIDMode m_mode = PIDMode.PID_VELOCITY;

  private double driveDistance_Feet = 0.0;
  private CANSparkMax m_rightMotor;
  private CANPIDController m_rightMotorPid;
  private CANEncoder m_rightEncoder;
  private JoystickButton m_HighShot;
  private JoystickButton m_LowShot;

  private Shooter() {
    // initialize Neo motor
    m_leftMotor = new CANSparkMax(Constants.canAddr_leftShooter, MotorType.kBrushless);
    m_rightMotor = new CANSparkMax(Constants.canAddr_rightShooter, MotorType.kBrushless);

    /**
     * The RestoreFactoryDefaults method can be used to reset the configuration
     * parameters in the SPARK MAX to their factory default state. If no argument is
     * passed, these parameters will not persist between power cycles
     */
    m_leftMotor.restoreFactoryDefaults();
    m_rightMotor.restoreFactoryDefaults();
    m_leftMotor.setIdleMode(IdleMode.kBrake);
    m_rightMotor.setIdleMode(IdleMode.kBrake);

    // m_rightMotor.follow(m_leftMotor);

    m_rightMotor.setInverted(false);
    m_leftMotor.setInverted(false);

    /**
     * In order to use PID functionality for a controller, a CANPIDController object
     * is constructed by calling the getPIDController() method on an existing
     * CANSparkMax object
     */
    m_leftMotorPid = m_leftMotor.getPIDController();
    m_rightMotorPid = m_rightMotor.getPIDController();

    // Encoder object created to display position values
    m_leftEncoder = m_leftMotor.getEncoder();
    m_rightEncoder = m_rightMotor.getEncoder();

    // PID Velocity control coefficients
    kP = 5e-5;
    kI = 1e-6;
    kD = 0;
    kIz = 0;
    kFF = 0;
    kMaxOutput = 1;
    kMinOutput = -1;
    maxRPM = 5700;

    // set Velocity PID coefficients
    m_leftMotorPid.setP(kP, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setI(kI, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setD(kD, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setIZone(kIz, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setFF(kFF, PIDMode.PID_VELOCITY.value);
    m_leftMotorPid.setOutputRange(kMinOutput, kMaxOutput, PIDMode.PID_VELOCITY.value);

    m_rightMotorPid.setP(kP, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setI(kI, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setD(kD, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setIZone(kIz, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setFF(kFF, PIDMode.PID_VELOCITY.value);
    m_rightMotorPid.setOutputRange(kMinOutput, kMaxOutput, PIDMode.PID_VELOCITY.value);

    register();
  }

  @Override
  public void periodic() {
    periodicPidTuning();

    periodicVelocity();
  }

  private double cmdPercentRpm = 0.0;

  public void periodicVelocity() {
    final double minPercentRpm = 0.05;
    if (m_HighShot.get()) {
      cmdPercentRpm = highShotSpeed_Pcnt;
    } else if (m_LowShot.get()) {
      cmdPercentRpm = lowShotSpeed_Pcnt;
    } else {
      cmdPercentRpm = 0.0;
    }

    /*
     * If the stick command is really close to 0, just return 0; otherwise return
     * the stick command make sure that it works for forward and backward commands,
     * so check the absolute value
     */
    final double setPoint = (Math.abs(cmdPercentRpm) < minPercentRpm ? 0.0 : cmdPercentRpm) / 100.0 * maxRPM;
    SmartDashboard.putNumber("Shooter Setpoint", setPoint);
    if (setPoint != 0.0) {
      m_leftMotorPid.setReference(setPoint, ControlType.kVelocity, PIDMode.PID_VELOCITY.value);
      m_rightMotorPid.setReference(-setPoint, ControlType.kVelocity, PIDMode.PID_VELOCITY.value);
    } else {
      m_leftMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
      m_rightMotorPid.setReference(0, ControlType.kVoltage, PIDMode.PID_VOLTAGE.value);
    }
    testperiodicVelocity();
  }

  public void setHighShot(final JoystickButton highShot) {
    m_HighShot = highShot;
  }

  public boolean getIsShooting() {
    final double cmdRpm = cmdPercentRpm * 5700 / 100.0;
    final double rpm = m_leftEncoder.getVelocity();
    final double error = cmdRpm - rpm;
    final double errorPcnt = error / cmdRpm;

    if (cmdPercentRpm < 0.1) {
      return false;
    } else if (errorPcnt < 0.1) {
      return true;
    } else {
      return false;
    }
  }

  public void setLowShot(final JoystickButton lowShot) {
    m_LowShot = lowShot;
  }

  private boolean notRunYet = true;

  public void periodicPidTuning() {
    if (!RobotState.isTest()) {
      return;
    }
    if (notRunYet) {
      // display PID coefficients on SmartDashboard
      SmartDashboard.putNumber("P Gain", kP);
      SmartDashboard.putNumber("I Gain", kI);
      SmartDashboard.putNumber("D Gain", kD);
      SmartDashboard.putNumber("I Zone", kIz);
      SmartDashboard.putNumber("Feed Forward", kFF);
      SmartDashboard.putNumber("Max Output", kMaxOutput);
      SmartDashboard.putNumber("Min Output", kMinOutput);
      SmartDashboard.putBoolean("PID: VEL", m_mode == PIDMode.PID_VELOCITY);
      SmartDashboard.putNumber("Drive Dist (Ft)", 0.0);

      SmartDashboard.putNumber("highShotSpeed_%", highShotSpeed_Pcnt);
      SmartDashboard.putNumber("lowShotSpeed_%", lowShotSpeed_Pcnt);
      notRunYet = false;
    }
    // read PID coefficients from SmartDashboard
    final double p = SmartDashboard.getNumber("P Gain", 0);
    final double i = SmartDashboard.getNumber("I Gain", 0);
    final double d = SmartDashboard.getNumber("D Gain", 0);
    final double iz = SmartDashboard.getNumber("I Zone", 0);
    final double ff = SmartDashboard.getNumber("Feed Forward", 0);
    final double max = SmartDashboard.getNumber("Max Output", 0);
    final double min = SmartDashboard.getNumber("Min Output", 0);

    // if PID coefficients on SmartDashboard have changed, write new values to
    // controller
    if ((p != kP)) {
      m_leftMotorPid.setP(p);
      kP = p;
    }
    if ((i != kI)) {
      m_leftMotorPid.setI(i);
      kI = i;
    }
    if ((d != kD)) {
      m_leftMotorPid.setD(d);
      kD = d;
    }
    if ((iz != kIz)) {
      m_leftMotorPid.setIZone(iz);
      kIz = iz;
    }
    if ((ff != kFF)) {
      m_leftMotorPid.setFF(ff);
      kFF = ff;
    }
    if ((max != kMaxOutput) || (min != kMinOutput)) {
      m_leftMotorPid.setOutputRange(min, max);
      kMinOutput = min;
      kMaxOutput = max;
    }

    highShotSpeed_Pcnt = SmartDashboard.getNumber("highShotSpeed_%", 0.0) / 100.0;
    lowShotSpeed_Pcnt = SmartDashboard.getNumber("lowShotSpeed_%", 0.0) / 100.0;

  }

  private void testperiodicVelocity() {
    if (!RobotState.isTest()) {
      // return;
    }
    final double motorRpm = m_leftEncoder.getVelocity();
    // SmartDashboard.putNumber("Velocity SetPoint", setPoint);
    SmartDashboard.putNumber("Velocity", motorRpm);
    // SmartDashboard.putNumber("Velocity Error", setPoint - motorRpm);
  }
}