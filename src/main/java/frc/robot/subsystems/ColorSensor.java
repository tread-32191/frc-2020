/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj.util.ColorShim;

/**
 * Add your docs here.
 */
public class ColorSensor extends SubsystemBase {
  // Constants

  public class ColorProps {
    String colorLabel = "";
    double redMin = 0;
    double redMax = 0;
    double greenMin = 0;
    double greenMax = 0;
    double blueMin = 0;
    double blueMax = 0;
    boolean isColor = false;

    @Override
    public String toString() {
      return colorLabel;
    }
  }

  private final ColorSensorV3 m_colorSensor;
  private Color m_detectedColor = new ColorShim(0, 0, 0);

  public ColorSensor(final I2C.Port i2cPort) {
    /**
     * A Rev Color Sensor V3 object is constructed with an I2C port as a parameter.
     * The device will be automatically initialized with default parameters.
     */
    m_colorSensor = new ColorSensorV3(i2cPort);

    register();
  }

  public void whatIsMyColor(final ColorProps[] myColorProps) {
    final double blueSense = m_detectedColor.blue;
    final double redSense = m_detectedColor.red;
    final double greenSense = m_detectedColor.green;
    boolean foundColor = false;

    for (final ColorProps colorProp : myColorProps) {
      colorProp.isColor = false;
      if (foundColor)
        continue;
      SmartDashboard.putString("Detected colour", "?");

      if ((blueSense > colorProp.blueMin) && (blueSense < colorProp.blueMax) && (greenSense > colorProp.greenMin)
          && (greenSense < colorProp.greenMax) && (redSense > colorProp.redMin) && (redSense < colorProp.redMax)) {
        colorProp.isColor = true;
        foundColor = true;
        SmartDashboard.putString("Detected colour", colorProp.toString());
      }
    }
  }
  // todo move to control panel spinner thing
  // And then it will pass in its color props to this class

  @Override
  public void periodic() {
    if (m_state == ColorSensorState.ON) {
      m_detectedColor = m_colorSensor.getColor();
      // stay in this state unless someone calls reset
    } else if (m_state == ColorSensorState.CLOSE_WAIT) {
      m_state = ColorSensorState.OPENING;
    } else if (m_state == ColorSensorState.OPENING) {
      // m_colorSensor.open();
      m_state = ColorSensorState.OPEN_WAIT;
    } else if (m_state == ColorSensorState.OPEN_WAIT) {
      m_state = ColorSensorState.ON;
    }

    testPeriodic();

  }

  private enum ColorSensorState {
    ON(0), CLOSE(1), CLOSE_WAIT(2), OPENING(3), OPEN_WAIT(4);

    public final int value;

    private ColorSensorState(final int value) {
      this.value = value;
    }
  }

  private ColorSensorState m_state = ColorSensorState.ON;

  public void reset() {
    m_state = ColorSensorState.CLOSE;
    // m_colorSensor.close();
  }

  private void testPeriodic() {
    if (!RobotState.isTest()) {
      return;
    }
    /**
     * TODO: Debug only consider deleting The sensor returns a raw IR value of the
     * infrared light detected.
     */
    double IR = m_colorSensor.getIR();
    SmartDashboard.putNumber("IR", IR);

    /**
     * Open Smart Dashboard or Shuffleboard to see the color detected by the sensor.
     */
    SmartDashboard.putNumber("Red", m_detectedColor.red);
    SmartDashboard.putNumber("Green", m_detectedColor.green);
    SmartDashboard.putNumber("Blue", m_detectedColor.blue);
  }

  public double getProximity() {
    final double proximity = m_colorSensor.getProximity();
    SmartDashboard.putNumber("Proximity", proximity);
    return proximity;
  }

}
