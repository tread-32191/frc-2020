/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.NeutralMode;

/**
 * I think we're done with this
 */
//
public class Hopper extends SubsystemBase {
  private static Hopper m_instance;
  private final AllenBradleySensor shooterBallDetector;
  private boolean m_ballAtShooter = false;
  private int m_ballShot = 0;
  private final AllenBradleySensor intakeBallSensor;
  private final WPI_TalonSRX conveyor = new WPI_TalonSRX(Constants.canAddr_conveyor);
  private final Timer m_Timer = new Timer();
  private int m_ballsAtStartOfMatch = 3;
  private boolean m_ballAtIntake;
  private int m_ballsPickedUp;
  private JoystickButton m_resetBalls;

  private Hopper() {
    conveyor.clearStickyFaults(0);
    conveyor.configFactoryDefault();
    conveyor.setNeutralMode(NeutralMode.Brake);
    conveyor.setInverted(false);
    m_Timer.start();
    intakeBallSensor = new AllenBradleySensor(Constants.ai_HopperIntake);
    shooterBallDetector = new AllenBradleySensor(Constants.ai_HopperShooter);
    register();
  }

  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  public static Hopper getInstance() {
    if (m_instance == null) {
      m_instance = new Hopper();
    }
    return m_instance;
  }

  @Override
  public void periodic() {
    m_ballsAtStartOfMatch = (int) SmartDashboard.getNumber("Balls @ Start", m_ballsAtStartOfMatch);
    if (m_resetBalls.get()) {
      m_ballsPickedUp = m_ballShot - m_ballsAtStartOfMatch;
      if (m_ballsPickedUp < 0) {
        m_ballsPickedUp = 0;
      }
    }
    final boolean ballAtShooter = shooterBallDetector.get();
    if (m_ballAtShooter == false && ballAtShooter == true) {
      if (m_ballShot >= m_ballsPickedUp + m_ballsAtStartOfMatch) {
        m_ballShot = m_ballsPickedUp + m_ballsAtStartOfMatch;
      } else {
        m_ballShot++;
      }
    }
    m_ballAtShooter = ballAtShooter;

    final boolean ballAtIntake = intakeBallSensor.get();
    if (m_ballAtIntake == false && ballAtIntake == true) {
      m_ballsPickedUp++;
    }
    m_ballAtIntake = ballAtIntake;

    if (intakeBallSensor.get()) {
      m_Timer.reset();
      /* restart timer */
    }
    if (m_Timer.get() < 0.25) {
      conveyor.set(0.8);
    } else if ((Shooter.getInstance().getIsShooting())) {
      conveyor.set(1.0);
    } else {
      conveyor.set(0);
    }
    Intake.getInstance().isHopperFull(amIFull());

    testPeriodic();
  }

  private boolean amIFull() {
    final int numberOfBallsIHave = m_ballsPickedUp + m_ballsAtStartOfMatch - m_ballShot;
    final int numberOfBallsICanHold = 5;
    return numberOfBallsIHave >= numberOfBallsICanHold;
  }

  private void testPeriodic() {
    if (!RobotState.isTest()) {
      return;
    }
    SmartDashboard.putBoolean("m_ballAtShooter", m_ballAtShooter);
    SmartDashboard.putNumber("m_ballShot", m_ballShot);
    SmartDashboard.putNumber("Ball Count", m_ballsPickedUp + m_ballsAtStartOfMatch - m_ballShot);
  }

  public void setResetBalls(JoystickButton joystickButton) {
    m_resetBalls = joystickButton;
  }

}