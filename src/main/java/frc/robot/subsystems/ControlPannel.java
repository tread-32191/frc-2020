/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.DriverStation;
import frc.robot.Constants;
import frc.robot.subsystems.ColorSensor;
import frc.robot.subsystems.ColorSensor.ColorProps;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

//TODO we still don't have a motor control
// detect the color and spin it to that color
// spin three revolutions - spin for a certain time or detect a color next to it 
// we have to put the motor up and then put it back down

public class ControlPannel extends SubsystemBase {
  private ColorProps[] myColorProps;
  private static ControlPannel m_instance;
  private ColorSensor m_colorSensor;
  private JoystickButton m_controlPanelButton;
  private WPI_TalonSRX m_controlPanelmotor = new WPI_TalonSRX(Constants.canAddr_controlPanelSpinner);
  private final ShooterElevator m_shooterElevator = ShooterElevator.getInstance();

  private int m_initialColor = -1;
  private int m_currentColor = -1;
  private int m_nextColor = -1;
  private double m_score = 0.0;
  private boolean buttonWasOn = false;
  private boolean buttonEdgedOn = false;

  /**
   * Creates a new ControlPannel.
   */

  private ControlPannel() {
    m_colorSensor = new ColorSensor(I2C.Port.kOnboard);
    init();
    register();
  }

  /*
   * spin a motor stop on a color if color is missed, then spin back @ half speed
   * 
   * 
   */
  public static ControlPannel getInstance() {
    if (m_instance == null) {
      m_instance = new ControlPannel();
    }
    return m_instance;
  }

  // DO NOT CHANGE THE ORDER OF THESE COLOR NUMBERS
  private final static int RED = 0;
  private final static int YELLOW = 1;
  private final static int BLUE = 2;
  private final static int GREEN = 3;
  private final static int NUM_COLORS = 4;
  private final static int FIRST_COLOR = 0;
  private final static int INVALID_COLOR = -1;

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    m_colorSensor.whatIsMyColor(myColorProps);

    String gameData;

    gameData = DriverStation.getInstance().getGameSpecificMessage();
    int fmsColor = INVALID_COLOR;
    if (gameData.length() > 0) {
      switch (gameData.toUpperCase().charAt(0)) {
      case 'B':
        fmsColor = BLUE;
        // Blue case code
        break;
      case 'G':
        fmsColor = GREEN;
        // Green case code
        break;
      case 'R':
        fmsColor = RED;
        // Red case code
        break;
      case 'Y':
        fmsColor = YELLOW;
        // Yellow case code
        break;
      default:
        // This is corrupt data
        break;
      }
    } else {
      // Code for no data received yet
    }

    final boolean button_on = m_controlPanelButton.get();
    buttonEdgedOn = button_on && buttonWasOn;
    buttonWasOn = button_on;
    if (buttonEdgedOn) {
      m_colorSensor.reset();
    }

    // Spin wheel three times
    if (fmsColor == INVALID_COLOR) {
      spin();
    } else {
      // Spin to color and stop
      final int fmsColorOffset = 2;
      int fieldDetectedColor = getColorOffset(fmsColor, fmsColorOffset);
      if (m_controlPanelButton.get() && !myColorProps[fieldDetectedColor].isColor) {
        m_controlPanelmotor.set(0.25);
      } else {
        m_controlPanelmotor.set(0.0);
      }
    }
    if (m_controlPanelmotor.get() > 0.0) {
      m_shooterElevator.goToMax();
    }
  }

  private double spin() {
    if (m_controlPanelButton.get()) {
      if (m_initialColor < FIRST_COLOR) {
        for (int idx = FIRST_COLOR; idx < NUM_COLORS; idx++) {
          if (myColorProps[idx].isColor) {
            m_initialColor = idx;
            m_currentColor = m_initialColor;
            m_nextColor = getColorOffset(m_currentColor, 1);
            break;
          }
        }
      } else {
        if (myColorProps[m_nextColor].isColor) {
          m_currentColor = m_nextColor;
          m_nextColor = getColorOffset(m_nextColor, 1);
          m_score += 0.125;
        }
      }
      if (m_score < 3.5) {
        m_controlPanelmotor.set(0.30);
      } else {
        m_controlPanelmotor.set(0.0);
      }
    } else {
      m_score = 0;
      m_initialColor = FIRST_COLOR - 1;
      m_controlPanelmotor.set(0.0);
    }
    return m_score;
  }

  private int getColorOffset(final int color, final int offset) {
    return (color + offset) % NUM_COLORS;
  }

  private void init() {
    myColorProps = new ColorSensor.ColorProps[NUM_COLORS];

    myColorProps[RED] = m_colorSensor.new ColorProps();
    myColorProps[RED].colorLabel = "RED";
    myColorProps[RED].blueMin = 0.04;
    myColorProps[RED].blueMax = 0.22;
    myColorProps[RED].redMin = 0.34;
    myColorProps[RED].redMax = 0.70;
    myColorProps[RED].greenMin = 0.23;
    myColorProps[RED].greenMax = 0.45;

    myColorProps[YELLOW] = m_colorSensor.new ColorProps();
    myColorProps[YELLOW].colorLabel = "YELLOW";
    myColorProps[YELLOW].blueMin = 0.05;
    myColorProps[YELLOW].blueMax = 0.19;
    myColorProps[YELLOW].redMin = 0.29;
    myColorProps[YELLOW].redMax = 0.42;
    myColorProps[YELLOW].greenMin = 0.44;
    myColorProps[YELLOW].greenMax = 0.57;

    myColorProps[GREEN] = m_colorSensor.new ColorProps();
    myColorProps[GREEN].colorLabel = "GREEN";
    myColorProps[GREEN].blueMin = 0.17;
    myColorProps[GREEN].blueMax = 0.27;
    myColorProps[GREEN].redMin = 0.15;
    myColorProps[GREEN].redMax = 0.33;
    myColorProps[GREEN].greenMin = 0.48;
    myColorProps[GREEN].greenMax = 0.67;

    myColorProps[BLUE] = m_colorSensor.new ColorProps();
    myColorProps[BLUE].colorLabel = "BLUE";
    myColorProps[BLUE].blueMin = 0.26;
    myColorProps[BLUE].blueMax = 0.48;
    myColorProps[BLUE].redMin = 0.10;
    myColorProps[BLUE].redMax = 0.25;
    myColorProps[BLUE].greenMin = 0.415;
    myColorProps[BLUE].greenMax = 0.495;
  }

  public void setControlPanelButton(final JoystickButton controlPanelButton) {
    m_controlPanelButton = controlPanelButton;
  }
}
