/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.Command;
import frc.robot.subsystems.ControlPannel;
import frc.robot.subsystems.DriveSystem;
import frc.robot.subsystems.Hopper;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Lightsaber;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.ShooterElevator;
import frc.robot.subsystems.UltrasonicSensor;
import frc.robot.subsystems.Winch;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  public XboxController gunnerstick = new XboxController(2);
  // The robot's subsystems and commands are defined here...
  // private final ExampleCommand m_autoCommand = new
  // ExampleCommand(m_exampleSubsystem);
  private DriveSystem m_drive;

  private Shooter m_shooter;
  private ControlPannel m_ControlPanel;
  private Hopper m_Hopper;
  private Intake m_Intake;
  private Lightsaber m_Lightsaber;
  private Winch m_Winch;
  private Object m_DriveSystem;
  private UltrasonicSensor m_ultraSonicSensor;

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {

    m_drive = DriveSystem.getInstance();
    final Joystick leftDrivestick = new Joystick(0);
    final Joystick rightDrivestick = new Joystick(1);
    m_drive.setLeftDriveStick(leftDrivestick);
    m_drive.setRightDriveStick(rightDrivestick);
    final JoystickButton fastspeedleft = new JoystickButton(leftDrivestick, 1);
    final JoystickButton fastspeedright = new JoystickButton(rightDrivestick, 1);
    final JoystickButton reverseToggle = new JoystickButton(rightDrivestick, 2);
    m_drive.setfastspeedleft(fastspeedleft);
    m_drive.setfastspeedright(fastspeedright);
    m_drive.setReverseToggle(reverseToggle);

    m_shooter = Shooter.getInstance();
    final JoystickButton highShot = new JoystickButton(gunnerstick, Constants.gunnerButton_LB);
    final JoystickButton lowShot = new JoystickButton(gunnerstick, Constants.gunnerButton_RB);
    m_shooter.setHighShot(highShot);
    m_shooter.setLowShot(lowShot);
    final ShooterElevator m_shooterElevator = ShooterElevator.getInstance();
    m_shooterElevator.setController(gunnerstick);

    m_ControlPanel = ControlPannel.getInstance();
    final JoystickButton controlPanelButton = new JoystickButton(gunnerstick, Constants.gunnerButton_Back);
    m_ControlPanel.setControlPanelButton(controlPanelButton);
    m_Hopper = Hopper.getInstance();
    m_Intake = Intake.getInstance();
    m_Intake.setManualRun(new JoystickButton(gunnerstick, Constants.gunnerButton_A));
    m_Intake.setAutoButton(new JoystickButton(gunnerstick, Constants.gunnerButton_X));
    m_Intake.setManualStop(new JoystickButton(gunnerstick, Constants.gunnerButton_B));
    m_Hopper.setResetBalls(new JoystickButton(gunnerstick, Constants.gunnerButton_Start));

    m_Lightsaber = Lightsaber.getInstance();
    m_Lightsaber.setController(gunnerstick);
    m_Winch = Winch.getInstance();
    m_Winch.setController(gunnerstick);
    m_Winch.setClawCloseButton(new JoystickButton(gunnerstick, Constants.gunnerButton_Y));
    // Configure the button bindings configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by instantiating a {@link GenericHID} or one of its subclasses
   * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
   * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {

  }

  // public Command getAutonomousCommand() {

  // }
  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  // public Command getAutonomousCommand() {
  // An ExampleCommand will run in autonomous
  // return m_autoCommand;
  // }
}
