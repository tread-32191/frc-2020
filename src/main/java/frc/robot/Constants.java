/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final int gunnerButton_A = 1;
    public static final int gunnerButton_B = 2;
    public static final int gunnerButton_X = 3;
    public static final int gunnerButton_Y = 4;
    public static final int gunnerButton_LB = 5;
    public static final int gunnerButton_RB = 6;
    public static final int gunnerButton_Back = 7;
    public static final int gunnerButton_Start = 8;
    public static final int gunnerButton_LeftStick = 9;
    public static final int gunnerButton_RightStick = 10;

    public static final int canAddr_rightFrontDrive = 1;
    public static final int canAddr_leftFrontDrive = 2;
    public static final int canAddr_rightRearDrive = 3;
    public static final int canAddr_leftRearDrive = 4;
    public static final int canAddr_leftShooter = 5;
    public static final int canAddr_rightShooter = 6;
    public static final int canAddr_conveyor = 7;
    public static final int canAddr_winch = 8;
    public static final int canAddr_intake = 9;
    public static final int canAddr_lightSaber = 10;
    public static final int canAddr_controlPanelSpinner = 11;
    public static final int canAddr_shooterElevator = 12;

    public static final int dio_HopperShooter = 12;
    public static final int dio_HopperIntake = 1;
    public static final int dio_rightLightsaberFwdLimit = 2;
    public static final int dio_leftLightsaberFwdLimit = 3;
    public static final int dio_rightLightsaberBkdLimit = 4;
    public static final int dio_leftLightsaberBkdLimit = 5;
    public static final int dio_encoderChannelA_shooterElevator = 6;
    public static final int dio_encoderChannelB_shooterElevator = 7;
    public static final int dio_encoderChannelA_winch = 8;
    public static final int dio_encoderChannelB_winch = 9;
    public static final int dio_encoderChannelA_lightsaber = 10;
    public static final int dio_encoderChannelB_lightsaber = 11;

    public static final int ai_HopperIntake = 0;
    public static final int ai_HopperShooter = 1;
    public static final int ai_ultraSonic_X = 2;
    public static final int ai_ultraSonic_Y = 3;
    public static final int ai_controlPanelDetector = 4;

}
